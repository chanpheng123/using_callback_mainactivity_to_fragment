package com.example.ksg_usr.cb_example;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    public static MyCallBack myCallBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.myViewPager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCallBack.changeText("Hello");
            }
        });
    }


    interface MyCallBack{
        void changeText(String str);
    }
}
